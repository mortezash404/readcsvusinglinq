﻿using System;
using System.Collections;
using System.IO;
using System.Linq;

namespace LinqConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var enumerable = File.ReadAllLines("C:\\Users\\Nikan\\Desktop\\person.csv").Select(s =>
            {
                var split = s.Split(',');

                var person = new Person
                {
                    Name = split[0],
                    Age = split[1],
                    Tell = split[2]
                };

                return person;

            }).Skip(1).ToList();

            Console.WriteLine("Name, Age, Tell");

            foreach (var person in enumerable)
            {
                Console.WriteLine($"{person.Name}, {person.Age}, {person.Tell}");
            }
        }
    }
}
