﻿namespace LinqConsoleApp
{
    public class Person
    {
        public  string Name { get; set; }
        public  string Age { get; set; }
        public  string Tell { get; set; }
    }
}
